# Importing Libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import os
import xlsxwriter
import pyfiglet
import datetime as dt

#Defining File Path
pwd
path = os.getcwd()
os.getcwd()
#
#os.chdir()

# Reading in Data
#with open(r'data/USA_Housing.csv', mode ='r') as my_new_file:
#    contents = my_new_file.read()
##contents

USAhousing = pd.read_csv(r'data/USA_Housing.csv')
print(USAhousing.head())
print(USAhousing.info())
print(USAhousing.describe())
print(USAhousing.columns)



# EDA

print(sns.pairplot(USAhousing))
print(sns.distplot(USAhousing['Price']))
print(sns.heatmap(USAhousing.corr()))

# TRAINING
X = USAhousing[['Avg. Area Income', 'Avg. Area House Age', 'Avg. Area Number of Rooms',
               'Avg. Area Number of Bedrooms', 'Area Population']]
y = USAhousing['Price']


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=101)


## Creating and Training the Model
from sklearn.linear_model import LinearRegression
lm = LinearRegression()
lm.fit(X_train,y_train)



## Model Evaluation
# print the intercept
print(lm.intercept_)
